# Tenant Ingress (for sandboxes)

## How to use

### Existing domains

1) Find you .yaml file and change the front and engine `service` name field. ie. `name: feat-react-php-minds-front-sandbox`
2) Update the hostname
3) Push up a change and wait for argo to run
4) If argo doesn't run, visit https://argo.minds.com/applications/minds-sandbox-tenants-oke-sandbox-us-ashburn-1?resource= and click refresh

### A new custom domain

1) Create a new yaml file
2) Follow the steps above